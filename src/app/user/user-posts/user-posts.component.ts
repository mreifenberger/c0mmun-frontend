import {Component, Input, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {PostService} from '../../services/post.service';
import {Post} from '../../DTOs/post/Post';
import {User} from '../../DTOs/user/User';

@Component({
  selector: 'app-user-posts',
  templateUrl: './user-posts.component.html',
  styleUrls: ['./user-posts.component.css']
})
export class UserPostsComponent implements OnInit {

  @Input() selectedPost : Post;

  @Input() user : User;

  posts : Post[];

  subscription: Subscription;


  constructor(private postService: PostService) { }

  ngOnInit() {


  }

}
