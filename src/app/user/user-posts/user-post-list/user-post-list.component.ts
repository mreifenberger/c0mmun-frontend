import {Component, OnInit} from '@angular/core';
import {Post} from '../../../models/post.model';
import {PostService} from '../../../services/post.service';
import {User} from '../../../DTOs/user/User';
import {TokenStorage} from '../../../auth/token.storage';
import {HttpClient} from '@angular/common/http';
import {UrlProviderService} from '../../../services/urlProvider.service';
import {UserService} from '../../../services/user.service';

@Component({
  selector: 'app-user-post-list',
  templateUrl: './user-post-list.component.html',
  styleUrls: ['./user-post-list.component.css']
})
export class UserPostListComponent implements OnInit {

  posts: Post[] = [];


  constructor(private postService: PostService, private tokenStorage: TokenStorage, private httpClient: HttpClient, private urlProviderService: UrlProviderService, private userservice: UserService) {
  }

  ngOnInit() {
      this.postService.getUserPosts(this.tokenStorage.getUser()).subscribe((y: Response) => {
        this.posts = new Array<Post>();
        this.posts = (<Post[]>y['content']);
      });
  }

}
