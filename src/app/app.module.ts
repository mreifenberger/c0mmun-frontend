import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {WrapperComponent} from './wrapper/wrapper.component';
import {HeaderComponent} from './wrapper/header/header.component';
import {FooterComponent} from './wrapper/footer/footer.component';
import {BodyComponent} from './wrapper/body/body.component';
import {PostsComponent} from './posts/posts.component';
import {PostDetailComponent} from './posts/post-detail/post-detail.component';
import {PostListComponent} from './posts/post-list/post-list.component';
import {LoginComponent} from './auth/login/login.component';
import {RegisterComponent} from './auth/register/register.component';
import {
  AccordionModule,
  AutoCompleteModule,
  CalendarModule,
  CardModule,
  CarouselModule,
  ChartModule,
  CheckboxModule,
  ChipsModule,
  CodeHighlighterModule,
  ColorPickerModule,
  ConfirmDialogModule,
  ContextMenuModule,
  DialogModule,
  DropdownModule,
  EditorModule,
  FieldsetModule,
  FileUploadModule,
  GalleriaModule,
  GrowlModule,
  InplaceModule,
  InputMaskModule,
  InputSwitchModule,
  InputTextareaModule,
  InputTextModule,
  LightboxModule,
  ListboxModule,
  MegaMenuModule,
  MenubarModule,
  MenuModule,
  MessageModule,
  MessagesModule,
  MultiSelectModule,
  OrderListModule,
  OrganizationChartModule,
  OverlayPanelModule,
  PaginatorModule,
  PanelMenuModule,
  PanelModule,
  PasswordModule,
  PickListModule,
  ProgressBarModule,
  ProgressSpinnerModule,
  RadioButtonModule,
  RatingModule,
  ScheduleModule,
  ScrollPanelModule,
  SelectButtonModule,
  SlideMenuModule,
  SliderModule,
  SpinnerModule,
  SplitButtonModule,
  StepsModule,
  TabMenuModule,
  TabViewModule,
  TerminalModule,
  TieredMenuModule,
  ToggleButtonModule,
  ToolbarModule,
  TooltipModule,
  TreeModule,
  TreeTableModule
} from 'primeng/primeng';
import {ButtonModule} from 'primeng/button';
import {PostService} from './services/post.service';
import {AppRoutingModule} from './app-routing.module';
import {IdService} from './services/id.service';
import {RouterModule} from '@angular/router';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {UserService} from './services/user.service';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {AuthService} from './auth/auth.service';
import {Interceptor} from './app.interceptor';
import {TokenStorage} from './auth/token.storage';
import {FileUploadComponent} from './file-upload/file-upload.component';
import {DataViewModule} from 'primeng/dataview';
import {HttpModule} from '@angular/http';
import {TableModule} from 'primeng/table';
import {UserPostsComponent} from './user/user-posts/user-posts.component';
import {UserPostListComponent} from './user/user-posts/user-post-list/user-post-list.component';
import {UrlProviderService} from './services/urlProvider.service';
import {CategoryService} from './services/category.service';
import {FileUploadService} from './services/file-upload.service';
import {UserComponent} from './users/user/user.component';
import {PostItemComponent} from './posts/post-list/post-item/post-item.component';
import { ReportComponent } from './report/report.component';
import { InviteComponent } from './invite/invite.component';
import {UsersComponent} from './users/users.component';



@NgModule({
  declarations: [
    AppComponent,
    WrapperComponent,
    HeaderComponent,
    FooterComponent,
    BodyComponent,
    PostsComponent,
    PostDetailComponent,
    PostListComponent,
    PostItemComponent,
    LoginComponent,
    RegisterComponent,
    UserPostsComponent,
    FileUploadComponent,
    UserComponent,
    UsersComponent,
    UserPostListComponent,
    ReportComponent,
    InviteComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    CheckboxModule,
    ToolbarModule,
    ButtonModule,
    AppRoutingModule,
    RouterModule,
    ScrollPanelModule,
    PasswordModule,
    InputTextModule,
    InputSwitchModule,
    FileUploadModule,
    HttpClientModule,
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    AccordionModule,
    AutoCompleteModule,
    ButtonModule,
    CalendarModule,
    CarouselModule,
    ChartModule,
    CheckboxModule,
    ChipsModule,
    CodeHighlighterModule,
    ConfirmDialogModule,
    ColorPickerModule,
    ContextMenuModule,
    DataViewModule,
    DialogModule,
    DropdownModule,
    EditorModule,
    FieldsetModule,
    FileUploadModule,
    GalleriaModule,
    GrowlModule,
    InplaceModule,
    InputMaskModule,
    InputSwitchModule,
    InputTextModule,
    InputTextareaModule,
    LightboxModule,
    ListboxModule,
    MegaMenuModule,
    MenuModule,
    MenubarModule,
    MessagesModule,
    MultiSelectModule,
    OrganizationChartModule,
    OrderListModule,
    OverlayPanelModule,
    PanelMenuModule,
    PaginatorModule,
    PanelModule,
    PickListModule,
    PasswordModule,
    ProgressBarModule,
    RadioButtonModule,
    ScheduleModule,
    RatingModule,
    ScrollPanelModule,
    SelectButtonModule,
    SlideMenuModule,
    SliderModule,
    SpinnerModule,
    SplitButtonModule,
    StepsModule,
    TableModule,
    TabMenuModule,
    TabViewModule,
    TerminalModule,
    TieredMenuModule,
    ToggleButtonModule,
    ToolbarModule,
    TooltipModule,
    TreeModule,
    TreeTableModule,
    CardModule,
    ReactiveFormsModule,
    CardModule,
    ReactiveFormsModule,
    HttpModule,
    MessageModule,
    ProgressSpinnerModule
  ],
  providers: [
    CategoryService,
    UserService,
    FileUploadService,
    PostService,
    IdService,
    UrlProviderService,
    UserService,
    AuthService,
    TokenStorage,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: Interceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
