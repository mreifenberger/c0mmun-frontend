import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params} from '@angular/router';
import {PostService} from '../../services/post.service';
import {Post} from '../../DTOs/post/Post';
import {Tag} from '../../DTOs/tag/Tag';
import {Comment} from '../../DTOs/comment/Comment';
import {Rating_Post_User} from '../../DTOs/user/Rating_Post_User';
import {TokenStorage} from '../../auth/token.storage';

@Component({
  selector: 'app-post-detail',
  templateUrl: './post-detail.component.html',
  styleUrls: ['./post-detail.component.css']
})
export class PostDetailComponent implements OnInit {

  id: number;
  post: Post;
  display = false;
  displayComment = false;

  public tagName = '';

  public commentText = '';

  addstring = '+ Add Tag';

  addstringComment = 'Create Comment';


  constructor(private postService: PostService,
              private route: ActivatedRoute,
              private tokenStorage: TokenStorage) { }

  ngOnInit() {
    this.post = new Post();

    this.route.params
      .subscribe(
        (params: Params) => {
          this.id = params['id'];
          this.postService.getPost(this.id).subscribe(x => { console.log(x); this.post = (<Post>x); console.log(this.post.tags); });
        }
      );
  }

  AddTag() {
    const tag: Tag = new Tag();
    tag.text = this.tagName;
    console.log(tag.text);
    this.postService.addTag(tag, this.post).subscribe(x => {
        console.log(x);
        this.post = (<Post>x);
        this.tagName = '';
        this.showDialog();
      }
    );
  }

  AddComment() {
    const comment: Comment = new Comment();
    comment.text = this.commentText;
    comment.user = this.tokenStorage.getUser();
    console.log(comment.text);
    this.postService.addComment(comment, this.post).subscribe(x => {
        console.log(x);
        this.post = (<Post>x);
        this.commentText = '';
        this.showDialogComment();
      }
    );
  }

  trackTag(index, tag) {
    return tag ? tag.id : undefined;

  }

  trackComment(index, comment) {
    return comment ? comment.id : undefined;

  }

  showDialog() {
    if (this.display === true) {
      this.display = false;
      this.addstring = '+ Add Tag';


    } else {
      this.display = true;
      this.addstring = '-';

    }
  }

  showDialogComment() {
    if (this.displayComment === true) {
      this.displayComment = false;
      this.addstringComment = 'Create Comment';

    } else {
      this.displayComment = true;
      this.addstringComment = '-';
    }
  }

  like() {
    this.postService.likePost(this.post).subscribe(x => {
      this.post = (<Rating_Post_User>x).id.post;
    });
    console.log('like');
  }

  dislike() {
    this.postService.dislikePost(this.post).subscribe(x => {
      this.post = (<Rating_Post_User>x).id.post;
    });
    console.log('dislike');
  }

}
