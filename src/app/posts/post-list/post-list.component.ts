import {AfterContentInit, AfterViewInit, Component, OnInit} from '@angular/core';
import {PostService} from '../../services/post.service';
import {CategoryService} from '../../services/category.service';
import {Post} from '../../DTOs/post/Post';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css']
})
export class PostListComponent implements OnInit {

  posts: Post[] = new Array<Post>();

  constructor(private postService: PostService, private categoryService: CategoryService) {
  }

  ngOnInit() {
    this.posts = new Array<Post>();
    this.categoryService.categoryChanged.subscribe(x => {
      this.postService.category = x;

      console.log(x);

      this.postService.getPostfromServer().subscribe((y: Response) => {
        this.posts = [];
        this.posts = (<Array<Post>>y['content']);
      });
    });
    }

  trackPost(index, post) {
    return post ? post.id : undefined;
  }
}
