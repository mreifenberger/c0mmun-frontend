import {Component, Input, OnInit} from '@angular/core';
import {Subscription} from 'rxjs/index';
import {PostService} from '../services/post.service';
import {Post} from '../DTOs/post/Post';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  @Input() selectedPost : Post;

  posts : Post[];

  subscription: Subscription;


  constructor(private postService: PostService) { }

  ngOnInit() {

  }

}
