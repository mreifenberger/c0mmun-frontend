export class IdService {
  private id = 0;

  getId(): number {
    this.id++;
    return this.id;
  }
}
