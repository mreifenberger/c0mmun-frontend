import {Injectable} from '@angular/core';
import {IdService} from './id.service';
import {HttpClient} from '@angular/common/http';
import {UrlProviderService} from './urlProvider.service';
import {Category} from '../DTOs/post/Category';
import {CategoryService} from './category.service';
import {User} from '../DTOs/user/User';
import {Tag} from '../DTOs/tag/Tag';
import {Comment} from '../DTOs/comment/Comment';
import {Tag_PostCreate} from '../DTOs/Tag_PostCreate';
import {Post} from '../DTOs/post/Post';
import {Rating_Post_User} from '../DTOs/user/Rating_Post_User';
import {Rating_Post_UserID} from '../DTOs/user/Rating_Post_UserID';
import {TokenStorage} from '../auth/token.storage';
import {Comment_PostCreate} from '../DTOs/Comment_PostCreate';

@Injectable()
export class PostService {

  posts: Post[];

  category: Category = this.categoryService.sfw;

  vari;

  // postsChanged : Subject<Post[]>

  constructor(private idService: IdService, private httpClient: HttpClient, private urlProviderService: UrlProviderService, private categoryService: CategoryService, private tokenStorage: TokenStorage) {
    // this.posts = new Array();
    // this.posts.push(new Post('https://media05.regionaut.meinbezirk.at/2015/12/18/9768490_web.jpg', new User( 'Opfi' ), [new Tag('testtag')], this.generateId()));
    // this.posts.push(new Post('http://www.nmsweitersfeld.ac.at/wp/wp-content/uploads/2017/05/LA-41.jpg', new User('StoalDasBaguette'), [new Tag('2ter tag'), new Tag('33ter tag')], this.generateId()));
  }


  getPostfromServer() {
    return this.httpClient.get(this.urlProviderService.url + '/Posts/Category/' + this.category.id);
  }


  getPost(index: number) {
    return this.httpClient.get(this.urlProviderService.url + '/Posts/Post/' + index);
  }

  addTag(tag: Tag, post: Post) {
    return this.httpClient.post(this.urlProviderService.url + '/Posts/AddTag', new Tag_PostCreate(tag, post) );
  }

  addComment(comment: Comment, post: Post) {
    return this.httpClient.post(this.urlProviderService.url + '/Posts/AddComment', new Comment_PostCreate(comment,post));
  }


  likePost(post: Post) {
    var ra_po_us: Rating_Post_User = new Rating_Post_User();
    var ra_po_us_id: Rating_Post_UserID = new Rating_Post_UserID();
    ra_po_us_id.post = post;
    ra_po_us_id.user = this.tokenStorage.getUser();

    ra_po_us.id = ra_po_us_id;
    ra_po_us.date = new Date();
    ra_po_us.value = 1;



    return this.httpClient.post(this.urlProviderService.url + '/Posts/RatePost', ra_po_us );
  }

  dislikePost(post: Post) {
    var ra_po_us: Rating_Post_User = new Rating_Post_User();
    var ra_po_us_id: Rating_Post_UserID = new Rating_Post_UserID();
    ra_po_us_id.post = post;
    ra_po_us_id.user = this.tokenStorage.getUser();


    ra_po_us.id = ra_po_us_id;
    ra_po_us.date = new Date();
    ra_po_us.value = -1;



    return this.httpClient.post(this.urlProviderService.url + '/Posts/RatePost', ra_po_us );
  }

  /*

  getPosts() {
    return this.posts.slice();
  }

  generateId(): number {
    return this.idService.getId();
  }
  */

  getUserPosts(user: User) {
      return this.httpClient.get(this.urlProviderService.url + '/Posts/User/' + user.id);
  }
}
