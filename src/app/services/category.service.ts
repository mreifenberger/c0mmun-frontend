import {EventEmitter, Injectable} from '@angular/core';
import {Category} from '../DTOs/post/Category';
import {BehaviorSubject} from 'rxjs';

@Injectable(
)
export class CategoryService {

  sfw:  Category = new Category();
  nsfw: Category = new Category();
  nsfl: Category = new Category();

  categoryChanged: BehaviorSubject<Category> = new BehaviorSubject(this.sfw);


  constructor() {
    this.sfw.name = 'sfw';
    this.sfw.id = 10;

    this.nsfw.name = 'nsfw';
    this.nsfw.id = 11;

    this.nsfl.name = 'nsfl';
    this.nsfl.id = 12;
  }
}
