import {Injectable} from '@angular/core';
import {HttpClient, HttpEvent, HttpHeaders, HttpRequest} from '@angular/common/http';
import {Observable, Subject} from 'rxjs';
import {Http} from '@angular/http';
import {UrlProviderService} from './urlProvider.service';
import {Post} from '../DTOs/post/Post';
import {UploadedFile} from '../DTOs/post/UploadedFile';
import {TokenStorage} from '../auth/token.storage';
import {Category} from '../DTOs/post/Category';
import {User} from '../DTOs/user/User';
import {UserService} from './user.service';

@Injectable({
  providedIn: 'root'
})
export class FileUploadService {
  public fileUploadCompleted: Subject<any> = new Subject<any>();

  post: Post;

  public fileUploaded(fileUploadObject: any) {
    this.fileUploadCompleted.next(fileUploadObject);
  }

  constructor(private httpClient: HttpClient, private http: Http, private urlProvider: UrlProviderService, private userservice: UserService) { }

  pushFileToStorage(file: File, name: string, categories: Array<Category>, user: User): Observable<HttpEvent<{}>> {
    this.post = new Post();
    this.post.user = user;
    this.post.file = new UploadedFile();
    this.post.name = name;
    this.post.categories = categories;
    this.post.file.uploadDate = new Date();

    console.log(this.post);
    const formdata: FormData = new FormData();


    formdata.append('img', file, file.name);
    formdata.append('post', new Blob([JSON.stringify(this.post)],
      {
        type: 'application/json'
      }));

    const req = new HttpRequest('POST', this.urlProvider.url + '/Posts/CreatePost', formdata, {
      reportProgress: true,
      responseType: 'text'
    });

    return this.httpClient.request(req);
  }




  b64toBlob(b64Data, contentType) {
    contentType = contentType || '';
    const sliceSize = 512;

    const byteCharacters = atob(b64Data);
    const byteArrays = [];

    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);

      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, {type: contentType});
    return blob;
  }

}
