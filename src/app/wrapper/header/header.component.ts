import {Component, OnInit} from '@angular/core';
import {SelectItem} from 'primeng/api';
import {CategoryService} from '../../services/category.service';
import {PostService} from '../../services/post.service';
import {Router} from "@angular/router";
import {TokenStorage} from "../../auth/token.storage";
import {UserService} from '../../services/user.service';
import {User} from '../../DTOs/user/User';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  showOnlyRecommendedPosts = false;

  sfw = true;
  nsfw = false;
  nsfl = false;
  sections: SelectItem[];

  user: User = new User();

  selectedSections: string[];

  constructor(private token: TokenStorage,
              private router: Router,
              private categoryService: CategoryService,
              private userservice: UserService) {
    this.sections = [
      {label: 'sfw', value: 'sfw'},
      {label: 'nsfw', value: 'nsfw'},
      {label: 'nsfl', value: 'nsfl'},
    ];
  }

  onCategoryChanged(name: string) {
    if (name === 'sfw') {
      this.categoryService.categoryChanged.next(this.categoryService.sfw);

    } else
      if (name === 'nsfw') {
        this.categoryService.categoryChanged.next(this.categoryService.nsfw);
      } else
      if (name === 'nsfl') {
        this.categoryService.categoryChanged.next(this.categoryService.nsfl);
      }

  }


  ngOnInit() {

    this.UserLoggedInOut();

    this.token.loggedInOutEvent.subscribe(() => {
      this.UserLoggedInOut();
    });

  }


  UserLoggedInOut() {
    if (this.token.getUser()) {
      this.user = this.token.getUser();
    } else {
      this.user.username = 'Not Logged In';
      console.log('not logged in?');
    }
  }

  logout() {
    this.token.signOut();
    this.router.navigate(['/login']);
  }

}
