import {Component, OnInit, ViewChild} from '@angular/core';
import {Report} from '../DTOs/user/Report';
import {ActivatedRoute} from "@angular/router";
import {TokenStorage} from "../auth/token.storage";
import {HttpClient} from "@angular/common/http";
import {User} from "../DTOs/user/User";

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})
export class ReportComponent implements OnInit {

  @ViewChild('f') form;

  reportedId: number;
  reportedUser: User;

  othersbool = false;

  others: string;

  constructor(private activeRoute: ActivatedRoute,
              private token: TokenStorage,
              private http: HttpClient) { }

  ngOnInit() {
    this.reportedId = +this.activeRoute.snapshot.paramMap.get('id');
    this.http.get('http://localhost:8080/Users/' + this.reportedId).subscribe(
      (response: User) => {this.reportedUser = response;
        console.log('loaded user to report: ');
        console.log(this.reportedUser);
      });
  }

  submit() {
    const r = new Report();
    r.description = this.form.value.report;
    r.reporter = this.token.getUser();
    r.reported = this.reportedUser;

    this.http.post('http://localhost:8080/Users/CreateReport', r).subscribe(
      response => {console.log(response);}
    );
  }

  onchange() {
    console.log(this.othersbool);
  }

}
