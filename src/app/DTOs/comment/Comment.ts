import {AEntity} from '../AEntity';
import {User} from '../user/User';

export class Comment extends AEntity {

  text: String;
  user: User;
  childComments: Array<Comment>;
}
