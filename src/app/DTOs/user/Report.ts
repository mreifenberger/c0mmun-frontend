import {AEntity} from '../AEntity';
import {User} from './User';

export class Report extends AEntity {
  reporter: User;
  description: string;
  reported: User;
}
