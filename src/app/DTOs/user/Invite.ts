import {AEntity} from '../AEntity';
import {User} from './User';

export class Invite extends AEntity {
  user: User;
  email: String;
}
