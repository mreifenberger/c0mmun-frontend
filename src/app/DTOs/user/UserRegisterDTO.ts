import {AEntity} from '../AEntity';

export class UserRegisterDTO extends AEntity {
  username: string;
  password: string;
  email: string;
  code: string;
}
