import {AEntity} from '../AEntity';
import {Tag} from '../tag/Tag';
import {User} from './User';

export class Rating_Tag_UserID {
  tag: Tag;
  user: User;
}
