import {Rating_Post_UserID} from './Rating_Post_UserID';

export class Rating_Post_User {
  id: Rating_Post_UserID;
  date: Date;
  value: number;
}
