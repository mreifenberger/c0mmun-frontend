import {Rating_Comment_UserID} from './Rating_Comment_UserID';
import {Data} from '@angular/router';

export class Rating_Comment_User {
  id: Rating_Comment_UserID;
  value: number;
  date: Data;
}
