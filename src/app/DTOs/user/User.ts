import {AEntity} from '../AEntity';
import {Post} from '../post/Post';
import {Invite} from './Invite';
import {Report} from './Report';
import {Role} from './Role';

export class User extends AEntity {
  email: string;
  username = '';
  password = '';
  rating: number;
  post: Post[] = [];
  comments: Comment[] = [];
  invites: Invite[] = [];
  recievedReports: Report[] = [];
  reports: Report[];
  active: boolean;
  roles: Role[] = [];
}
