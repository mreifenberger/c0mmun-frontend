import {AEntity} from '../AEntity';
import {Rating_Tag_UserID} from './Rating_Tag_UserID';

export class Rating_Tag_User {
  id: Rating_Tag_UserID;
  value: number;
  date: Date;
}
