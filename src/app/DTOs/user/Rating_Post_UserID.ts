import {AEntity} from '../AEntity';
import {Post} from '../post/Post';
import {User} from './User';

export class Rating_Post_UserID {
  post: Post;
  user: User;
}
