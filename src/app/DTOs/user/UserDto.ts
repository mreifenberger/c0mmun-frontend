import {AEntity} from '../AEntity';
import {Post} from '../post/Post';
import {Role} from './Role';

export class UserDto extends AEntity {
  username: string;
  rating: number;
  posts: Array<Post> = new Array<Post>();
  comments: Array<Comment> = new Array<Comment>();
  active: boolean;
  roles: Array<Role> = new Array<Role>();
}
