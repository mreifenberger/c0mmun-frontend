import {AEntity} from '../AEntity';

export class Role extends AEntity {
  id: number;
  name: String;
  description: String;
}
