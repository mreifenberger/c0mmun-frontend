import {AEntity} from '../AEntity';

export class Category extends AEntity {
  name: String;
}
