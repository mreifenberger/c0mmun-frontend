import {AEntity} from '../AEntity';
import {UploadedFile} from './UploadedFile';
import {Tag} from '../tag/Tag';
import {Category} from './Category';
import {User} from '../user/User';

export class Post extends AEntity {
  name: string;
  file: UploadedFile;
  rating: number = 0;
  tags: Array<Tag> = new Array<Tag>();
  comments: Array<Comment> = new Array<Comment>();
  categories: Array<Category> = new Array<Category>();
  user: User = new User();
}
