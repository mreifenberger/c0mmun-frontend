import {AEntity} from '../AEntity';
import {Post_CommentID} from './Post_CommentID';

export class Post_Comment {
  id: Post_CommentID;
  date: Date;
}
