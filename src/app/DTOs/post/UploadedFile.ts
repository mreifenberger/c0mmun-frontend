import {AEntity} from '../AEntity';

export class UploadedFile extends AEntity {
  file: Blob;
  uploadDate: Date;
}
