import {AEntity} from '../AEntity';
import {Post} from '../post/Post';

export class Tag extends AEntity {
  text: String;
  description: String;
  posts: Array<Post> = new Array<Post>();
}
