import {Tag} from './tag/Tag';
import {Post} from './post/Post';

export class Tag_PostCreate {

  tag: Tag;

  constructor(tag: Tag, post: Post) {
    this.tag = tag;
    this.post = post;
  }

  post: Post;

}
