import {Post} from './post/Post';
import {Comment} from '../DTOs/comment/Comment';


export class Comment_PostCreate {

  comment: Comment;

  constructor(comment: Comment, post: Post) {
    this.comment = comment;
    this.post = post;
  }

  post: Post;

}
