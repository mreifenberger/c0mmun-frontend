import { Injectable } from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router/src/router_state';
import {AuthService} from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(private auth: AuthService, private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
      console.log('authenticated:: ' + this.auth.isAuthenticated());
      if (this.auth.isAuthenticated()) {
        return true;
      } else {
        console.log('im else');
        this.router.navigate(['login']);
        return false;
      }
  }
}
