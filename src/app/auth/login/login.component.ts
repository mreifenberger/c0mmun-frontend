import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../auth.service';
import {TokenStorage} from '../token.storage';
import {UrlProviderService} from '../../services/urlProvider.service';
import {HttpClient} from '@angular/common/http';
import {User} from '../../DTOs/user/User';
import {UserService} from '../../services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username = '';
  password = '';

  ngOnInit() {
  }

  constructor(private router: Router, private authService: AuthService, private token: TokenStorage,
              private urlProviderService: UrlProviderService, private httpClient: HttpClient, private userservice:UserService) {
  }



  login(): void {
    this.authService.attemptAuth(this.username, this.password).subscribe(
      data => {
        this.token.saveToken(data.token);
        this.router.navigate(['posts']);

        this.httpClient.get(this.urlProviderService.url + '/Users/Username/' + this.username ).subscribe(x => {
          this.token.saveUser((<User>x));
        });
      }
    );
  }

}
