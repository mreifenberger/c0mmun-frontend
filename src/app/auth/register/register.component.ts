import {Component, OnInit, ViewChild} from '@angular/core';

import {NgForm} from '@angular/forms';
import {AuthService} from '../auth.service';
import {TokenStorage} from '../token.storage';
import {Router} from '@angular/router';
import {User} from '../../models/user.model';
import {UserRegisterDTO} from '../../DTOs/user/UserRegisterDTO';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  passwordRepeat;

  @ViewChild('f') f;

  constructor(private authService: AuthService,
              private token: TokenStorage,
              private router: Router) { }

  user: UserRegisterDTO = new UserRegisterDTO();

  ngOnInit() {
  }

  onSubmit(form) {
    if(this.isFormValid()) {
      this.authService.register(this.user).subscribe(response => {
        console.log('response');
        console.log(response);
        this.router.navigate(['login']);
      });
    }
  }

  isFormValid(): boolean {
    return this.passwordRepeat === this.user.password && this.f.valid;
  }

}
