import { Injectable } from '@angular/core';
import {User} from '../DTOs/user/User';
import {log} from 'util';
import {Subject} from 'rxjs';


const TOKEN_KEY = 'AuthToken';
const USER_KEY = 'User';

@Injectable()
export class TokenStorage {

  loggedInOutEvent: Subject<null> = new Subject();

  constructor() {
  }

  public saveUser(user: User) {
    window.sessionStorage.removeItem(USER_KEY);
    window.sessionStorage.setItem(USER_KEY, JSON.stringify(user));

    this.loggedInOutEvent.next();
  }

  public getUser() {
    log((<User>JSON.parse(window.sessionStorage.getItem(USER_KEY))));
    return (<User>JSON.parse(window.sessionStorage.getItem(USER_KEY)));
  }

  signOut() {
    window.sessionStorage.removeItem(TOKEN_KEY);
    window.sessionStorage.removeItem(USER_KEY);
    window.sessionStorage.clear();

    this.loggedInOutEvent.next();
  }

  public saveToken(token: string) {
    window.sessionStorage.removeItem(TOKEN_KEY);
    window.sessionStorage.setItem(TOKEN_KEY,  token);
  }

  public getToken(): string {
    return sessionStorage.getItem(TOKEN_KEY);
  }
}
