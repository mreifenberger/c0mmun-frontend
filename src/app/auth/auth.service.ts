import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/index';
import {TokenStorage} from './token.storage';
import {UserRegisterDTO} from '../DTOs/user/UserRegisterDTO';


@Injectable()
export class AuthService {

  baseUrl: 'http://localhost:8080/';

  constructor(private http: HttpClient, private token: TokenStorage) {
  }

  register(user: UserRegisterDTO) {

    return this.http.post('http://localhost:8080/signup', user);

  }

  attemptAuth(username: string, password: string): Observable<any> {
    const credentials = {username: username, password: password};
    console.log('attemptAuth ::');
    return this.http.post('http://localhost:8080/token/generate-token', credentials);
  }


  isAuthenticated() {
    console.log('token: ');
    console.log(this.token.getToken());
    return this.token.getToken() != null && this.token.getToken() !== '';
  }
}
