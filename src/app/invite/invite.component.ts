import { Component, OnInit } from '@angular/core';
import {User} from '../models/user.model';
import {MailInviteDTO} from '../DTOs/MailInviteDTO';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-invite',
  templateUrl: './invite.component.html',
  styleUrls: ['./invite.component.css']
})
export class InviteComponent implements OnInit {

  mailclient: MailInviteDTO = new MailInviteDTO();

  private user: User;

  constructor(private http: HttpClient) { }

  ngOnInit() {
  }

  send() {
    // invite im backend hinzufügen
    console.log(this.mailclient);
    const req = this.http.post('http://localhost:8080/sendmail', this.mailclient).subscribe(
      response => {console.log(response); console.log('response'); }
    );
  }

}
