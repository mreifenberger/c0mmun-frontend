import {Component, Input, OnInit} from '@angular/core';
import {HttpClient, HttpEventType, HttpResponse} from '@angular/common/http';
import {FileUploadService} from '../services/file-upload.service';
import {Category} from '../DTOs/post/Category';
import {CategoryService} from '../services/category.service';
import {User} from '../DTOs/user/User';
import {Post} from '../DTOs/post/Post';
import {Http} from '@angular/http';
import {UrlProviderService} from '../services/urlProvider.service';
import {UserService} from '../services/user.service';
import {TokenStorage} from '../auth/token.storage';
import {Router} from '@angular/router';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.css']
})
export class FileUploadComponent implements OnInit {

  categories: Array<Category> = [];

  sfw = false;
  nsfw = false;
  nsfl = false;

  status = '';

  display = false;

  agreedTherms = false;

  name;


  @Input()
  identifier = -1;

  selectedFiles: FileList;
  currentFileUpload: File;
  progress: { percentage: number } = { percentage: 0 };

  constructor(private fileUploadService: FileUploadService, private categoryService: CategoryService, private httpClient: HttpClient,
              private http: Http, private urlProvider: UrlProviderService, private userservice: UserService, private tokenstorage: TokenStorage, private router: Router) { }

  ngOnInit() {
  }

  categoriesArray(cat: string) {
    if (cat === 'sfw') {
      if (!this.categories.includes(this.categoryService.sfw)) {
        this.categories.push(this.categoryService.sfw);
        this.sfw = true;
      } else {
        this.categories.splice( this.categories.indexOf(this.categoryService.sfw), 1 );
        this.sfw = false;
      }
    }
    if (cat === 'nsfw') {
      if (!this.categories.includes(this.categoryService.nsfw)) {
        this.categories.push(this.categoryService.nsfw);
        this.nsfw = true;
      } else {
        this.categories.splice( this.categories.indexOf(this.categoryService.nsfw), 1 );
        this.nsfw = false;
      }
    }
    if (cat === 'nsfl') {
      if (!this.categories.includes(this.categoryService.nsfl)) {
        this.categories.push(this.categoryService.nsfl);
        this.nsfl = true;
      } else {
        this.categories.splice( this.categories.indexOf(this.categoryService.nsfl), 1 );
        this.nsfl = false;
      }
    }
  }

  selectFile(event) {
    this.selectedFiles = event.target.files;
  }

  upload() {
    this.progress.percentage = 0;
    this.currentFileUpload = this.selectedFiles.item(0);
    this.status = 'uploading...';
    this.fileUploadService.pushFileToStorage(this.currentFileUpload, this.name, this.categories, this.tokenstorage.getUser()).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress) {
        this.progress.percentage = Math.round(100 * event.loaded / event.total);
      } else if (event instanceof HttpResponse) {
        status = 'File is completely uploaded!';
        this.display = false;
        this.currentFileUpload = null;
        this.router.navigate(['/posts']);
      }
    });


    this.selectedFiles = undefined;
  }
/*
  fileUploadServiceCall(fileId: number) {
    this.fileUploadService.getFileFromStorage(fileId);
  }
*/
}
