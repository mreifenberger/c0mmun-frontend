import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {PostDetailComponent} from './posts/post-detail/post-detail.component';
import {PostsComponent} from './posts/posts.component';
import {LoginComponent} from './auth/login/login.component';
import {RegisterComponent} from './auth/register/register.component';
import {AuthGuardService} from './auth/auth-guard.service';
import {FileUploadComponent} from './file-upload/file-upload.component';
import {UserComponent} from './users/user/user.component';
import {ReportComponent} from './report/report.component';
import {InviteComponent} from './invite/invite.component';
import {UsersComponent} from './users/users.component';

const appRoutes: Routes = [
  { path: '', redirectTo: '/login', pathMatch : 'full'},
  { path: 'posts', component: PostsComponent, canActivate: [AuthGuardService], children : [
      { path: ':id', canActivate: [AuthGuardService], component: PostDetailComponent}
    ]},
  { path: 'login', component: LoginComponent},
  { path: 'register', component: RegisterComponent},
  { path: 'user', component: UsersComponent, children : [
      { path: ':userid', component: UserComponent, children : [
        ]}
    ]},
  { path: 'createPost', canActivate: [AuthGuardService], component: FileUploadComponent},
  {path: 'report/:id', canActivate: [AuthGuardService], component: ReportComponent},
  {path: 'invite', canActivate: [AuthGuardService], component: InviteComponent}
  ];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
