import {IdService} from "../services/id.service";
import {Tag} from "./tag.model";
import {User} from "./user.model";

export class Post {

  upvotes : number;

  //Besseren Namen ausdenken
  likethisPost : boolean;


  constructor(public imagePath : string,
              public user : User,
              public tags : Tag[],
              public id : number)
  {
    this.upvotes = 1;
    this.likethisPost = true;
  }

}
