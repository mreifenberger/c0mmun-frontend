export class UploadedFile {

  file: File;

  uploadDate: Date;
}
