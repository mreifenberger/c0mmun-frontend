import {Post} from './post.model';
import {Code} from '../DTOs/Code';

export class User {

  public email: string;
  public password: string;
  private upvotes: number;
  public posts: Post;
  public comments: Comment;
  private reports: any;
  private receivedReports: any;
  public active: boolean;
  public roles: any;



  constructor(public username: string) {}

  public toString = (): string => {

    return this.username;
  }


}
