import {User} from "./user.model";

export class Comment {

  constructor(public text:string,
              public parentComment: Comment,
              public author: User) {}

}
