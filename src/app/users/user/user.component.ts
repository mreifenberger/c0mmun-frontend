import { Component, OnInit } from '@angular/core';
import {User} from '../../DTOs/user/User';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {UrlProviderService} from '../../services/urlProvider.service';
import {Post} from '../../DTOs/post/Post';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  user: User;

  posts: Post[] = [];

  constructor(private activeRoute: ActivatedRoute,
              private router: Router,
              private httpClient: HttpClient,
              private urlProviderService: UrlProviderService) {
    this.user = new User();
   // this.user.upvotes = 0;
  }

  ngOnInit() {
    const id = this.activeRoute.snapshot.paramMap.get('userid');


    this.httpClient.get('http://localhost:8080/Posts/User/' + id).
    subscribe( response => {console.log(typeof(response)); });

    this.activeRoute.url.subscribe(url => {
      this.httpClient.get(this.urlProviderService.url + '/Users/' + id)
        .subscribe(x => {
          this.user = (<User>x);
        });
  });
  }


}
